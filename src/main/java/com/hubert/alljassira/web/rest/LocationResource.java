package com.hubert.alljassira.web.rest;

import com.hubert.alljassira.domain.LocationDto;
import com.hubert.alljassira.domain.LocationRequestDto;
import com.hubert.alljassira.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/locations")
@RequiredArgsConstructor
public class LocationResource {

    private final LocationService locationService;
    private final LocationMapperResource locationMapper;

    @PostMapping
    public ResponseEntity<?> save(@RequestBody LocationRequestDto locationRequestDto){
        var locationRequest = locationMapper.toDomain(locationRequestDto);
        var location = locationService.process(locationRequest);
        var locationDto = locationMapper.toDto(location);
        var locationUri = LocationUri.fromRequest(locationDto.getId());
        return ResponseEntity.created(locationUri).body(locationDto);
    }

    @GetMapping("{id}")
    public ResponseEntity<LocationDto> getAllLocations(@PathVariable String id) {
        return ResponseEntity.ok(null);
    }
}
