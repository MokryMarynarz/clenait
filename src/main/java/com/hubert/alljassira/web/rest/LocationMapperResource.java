package com.hubert.alljassira.web.rest;

import com.hubert.alljassira.domain.Location;
import com.hubert.alljassira.domain.LocationDto;
import com.hubert.alljassira.domain.LocationRequest;
import com.hubert.alljassira.domain.LocationRequestDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LocationMapperResource {
    LocationRequest toDomain(LocationRequestDto locationRequestDto);
    LocationDto toDto(Location location);

}
