package com.hubert.alljassira.domain;

import com.hubert.alljassira.repository.LocationRepository;
import com.hubert.alljassira.service.LocationService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LocationProcessor implements LocationService {

    private final LocationRepository locationRepository;
    private final IdGenerator locationIdGenerator;

    @Override
    public LocationEntity getById(String id) {
        return null;
    }

    @Override
    public Location process(LocationRequest locationRequest) {
        Location location = createLocation(locationRequest);
        return locationRepository.save(location);
    }

    private Location createLocation(LocationRequest locationRequest) {
        return Location.builder()
                .id(locationIdGenerator.getNext())
                .streetAddress(locationRequest.streetAddress)
                .postalCode(locationRequest.postalCode)
                .city(locationRequest.city)
                .stateProvince(locationRequest.stateProvince)
                .build();
    }
}
