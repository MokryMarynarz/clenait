package com.hubert.alljassira.domain;

import lombok.Data;

@Data
public class LocationRequest {
    String id;
    String streetAddress;
    String postalCode;
    String city;
    String stateProvince;
}
