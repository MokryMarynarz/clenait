package com.hubert.alljassira.domain;

import java.util.UUID;

class UuidGenerator implements IdGenerator {

    @Override
    public String getNext() {
        return UUID.randomUUID().toString();
    }

}
