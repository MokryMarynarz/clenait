package com.hubert.alljassira.domain.enumeration;

public enum WasteType {
    METAL,
    PLASTIC,
    GLASS,
    MIX,
}
