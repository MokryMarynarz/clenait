package com.hubert.alljassira.domain;

public interface IdGenerator {

    String getNext();

}
