package com.hubert.alljassira.domain;

import lombok.Data;

@Data
public class LocationDto {
    private String id;
    private String streetAddress;
    private String postalCode;
    private String city;
    private String stateProvince;
}
