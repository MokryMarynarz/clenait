package com.hubert.alljassira.domain;

import com.hubert.alljassira.domain.enumeration.WasteType;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "post")
public class Post implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "dump_type")
    private WasteType dumpType;

    @Column(name = "photo")
    private String photo;

    public Long getId() {
        return this.id;
    }

    public Post id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public Post title(String title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public Post description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public WasteType getDumpType() {
        return this.dumpType;
    }

    public Post dumpType(WasteType dumpType) {
        this.setDumpType(dumpType);
        return this;
    }

    public void setDumpType(WasteType dumpType) {
        this.dumpType = dumpType;
    }

    public String getPhoto() {
        return this.photo;
    }

    public Post photo(String photo) {
        this.setPhoto(photo);
        return this;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
