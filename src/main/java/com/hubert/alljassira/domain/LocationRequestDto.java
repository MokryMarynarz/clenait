package com.hubert.alljassira.domain;

import lombok.Data;

@Data
public class LocationRequestDto {
    //TODO validation
    Long requestId;
    String streetAddress;
    String postalCode;
    String city;
    String stateProvince;
}
