package com.hubert.alljassira.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * A Location.
 */
@Entity(name = "Location")
@Table
@Getter
@Setter
public class LocationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String streetAddress;
    private String postalCode;
    private String city;
    private String stateProvince;


}

