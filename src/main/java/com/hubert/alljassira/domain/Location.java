package com.hubert.alljassira.domain;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Location {
    String id;
    String streetAddress;
    String postalCode;
    String city;
    String stateProvince;
}
