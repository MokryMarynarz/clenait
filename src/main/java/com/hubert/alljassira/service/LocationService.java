package com.hubert.alljassira.service;

import com.hubert.alljassira.domain.Location;
import com.hubert.alljassira.domain.LocationEntity;
import com.hubert.alljassira.domain.LocationRequest;

public interface LocationService {
    LocationEntity getById(String id);
    Location process(LocationRequest locationRequest);
}
