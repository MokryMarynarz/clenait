package com.hubert.alljassira.service;

import com.hubert.alljassira.repository.LocationRepository;

public interface LocationServiceFactory  {
    LocationService create(LocationRepository locationRepository);

}
