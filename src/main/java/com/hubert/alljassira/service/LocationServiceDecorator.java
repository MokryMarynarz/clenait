package com.hubert.alljassira.service;

import com.hubert.alljassira.domain.Location;
import com.hubert.alljassira.domain.LocationEntity;
import com.hubert.alljassira.domain.LocationRequest;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LocationServiceDecorator implements LocationService{

    private final LocationService locationService;

    @Override
    public LocationEntity getById(String id) {
        return locationService.getById(id);
    }

    @Override
    public Location process(LocationRequest locationRequest) {
        return locationService.process(locationRequest);
    }
}
