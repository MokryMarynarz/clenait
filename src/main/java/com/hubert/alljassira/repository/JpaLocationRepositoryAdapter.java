package com.hubert.alljassira.repository;

import com.hubert.alljassira.domain.Location;
import com.hubert.alljassira.domain.LocationDto;
import com.hubert.alljassira.domain.LocationEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
@RequiredArgsConstructor
@Component
public class JpaLocationRepositoryAdapter implements LocationRepository {

    private final JpaLocationRepository jpaLocationRepository;
    private final JpaLocationMapper locationMapper;

    @Override
    public Location save(Location location) {
        LocationEntity locationEntity = locationMapper.toEntity(location);
        jpaLocationRepository.save(locationEntity);
        return locationMapper.toDomain(locationEntity);
    }

    @Override
    public Optional<Location> getById(String id) {
        return jpaLocationRepository.findById(id)
                .map(locationMapper::toDomain);
    }

}
