package com.hubert.alljassira.repository;

import com.hubert.alljassira.domain.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaLocationRepository extends JpaRepository<LocationEntity, String> {
}
