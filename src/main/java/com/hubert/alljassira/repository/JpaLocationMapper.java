package com.hubert.alljassira.repository;

import com.hubert.alljassira.domain.Location;
import com.hubert.alljassira.domain.LocationEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface JpaLocationMapper {
    LocationEntity toEntity(Location location);
    Location toDomain(LocationEntity location);
}
