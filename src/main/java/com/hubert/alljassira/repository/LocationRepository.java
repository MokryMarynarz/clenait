package com.hubert.alljassira.repository;

import com.hubert.alljassira.domain.Location;
import com.hubert.alljassira.domain.LocationEntity;

import java.util.Optional;

public interface LocationRepository {

    Location save(Location payment);

    Optional<Location> getById(String id);

}
