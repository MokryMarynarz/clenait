package com.hubert.alljassira;

import com.hubert.alljassira.domain.DefaultLocationServiceFactory;
import com.hubert.alljassira.repository.LocationRepository;
import com.hubert.alljassira.service.LocationService;
import com.hubert.alljassira.service.LocationServiceDecorator;
import com.hubert.alljassira.service.LocationServiceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class LocationsConfiguration {
    private static final LocationServiceFactory LOCATION_SERVICE_FACTORY = new DefaultLocationServiceFactory();

    @Bean
    public LocationService paymentService(LocationRepository locationRepository) {
        return LOCATION_SERVICE_FACTORY.create(locationRepository);
    }

    @Primary
    @Bean
    public LocationService paymentServiceDecorator(LocationService locationService) {
        return new LocationServiceDecorator(locationService);
    }
}
