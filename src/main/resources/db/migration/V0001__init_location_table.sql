DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS location;
-- drop drop
CREATE TABLE location(
                      id VARCHAR(255) PRIMARY KEY,
                      street_address VARCHAR(255) NOT NULL,
                      postal_code VARCHAR(255),
                      city VARCHAR(255) NOT NULL,
                      state_province VARCHAR(255)
);