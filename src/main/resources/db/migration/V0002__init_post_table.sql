CREATE TABLE post(
                  id BIGINT PRIMARY KEY,
                  title VARCHAR(255) NOT NULL,
                  description VARCHAR(255),
                  dump_type VARCHAR(255) NOT NULL,
                  photo VARCHAR(255),
                  location_id BIGINT
);