CREATE TABLE person(
                  id BIGINT PRIMARY KEY,
                  login VARCHAR(255) NOT NULL,
                  first_name VARCHAR(255),
                  last_name VARCHAR(255) NOT NULL,
                  email VARCHAR(255),
                  phone_number VARCHAR(255),
                  photo VARCHAR(255)
);